set_global_assignment -name VHDL_FILE ../../src/clockengen_rtl.vhd
set_global_assignment -name VHDL_FILE ../../src/hsync_rtl.vhdl
set_global_assignment -name VHDL_FILE ../../src/vsync_rtl.vhd
set_global_assignment -name VHDL_FILE ../../src/image_generator_rtl.vhd
set_global_assignment -name VHDL_FILE ../../src/de1_vga.vhd
set_global_assignment -name VHDL_FILE ../../src/uart_rx_rtl.vhd
