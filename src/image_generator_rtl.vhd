library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity image_generator is port (
	clk : in std_ulogic;
	en : in std_ulogic;
	rst_n: in std_logic;
	current_x_i : in unsigned(9 downto 0);
	current_y_i : in unsigned(9 downto 0);
	red_o: out std_ulogic_vector(3 downto 0);
	green_o: out std_ulogic_vector(3 downto 0);
	blue_o: out std_ulogic_vector(3 downto 0);
	ball_speed_up_i : in std_ulogic;
	ball_speed_down_i : in std_ulogic;
	ball_speed_left_i : in std_ulogic;
	ball_speed_right_i : in std_ulogic;
	step_i: in std_ulogic
); end entity;

architecture rtl of image_generator is
	signal rgb, rgb_new, rgb_ball : std_ulogic_vector(11 downto 0);
	signal ball_x, ball_y : unsigned(9 downto 0) := to_unsigned(0, 10);
	signal ball_x_new : unsigned(9 downto 0) := to_unsigned(0, 10);
	signal ball_y_new : unsigned(9 downto 0) := to_unsigned(0, 10);
	signal ball_speed_x, ball_speed_x_new : unsigned(9 downto 0) := to_unsigned(3, 10);
	signal ball_speed_y, ball_speed_y_new : unsigned(9 downto 0) := to_unsigned(3, 10);
	constant ball_size : unsigned(9 downto 0) := to_unsigned(20, 10);
	constant ball_speed_positive : unsigned(9 downto 0) := to_unsigned(3,    10);
	constant ball_speed_negative : unsigned(9 downto 0) := to_unsigned(1021, 10); -- using 1021 here since it is the same as to_unsigned(-3, 10)
	signal enable_bounce : std_ulogic;
	signal counter, counter_new : unsigned(9 downto 0) := to_unsigned(0, 10);
	signal counter_en : std_ulogic;
begin
	rgb <= "000000000000" when rst_n = '0' else rgb_new when rising_edge(clk);
	rgb_new <= rgb_ball when current_x_i < 640 and current_y_i < 480 else "000000000000";
	rgb_ball <= "000000001111" when current_x_i - ball_x < ball_size and current_y_i - ball_y < ball_size else "000011110000";
		
	counter_en <= '1';
	-- enable this to slow down the ball
	-- counter_en <= '1' when counter = 0 else '0':
	
	ball_y <= to_unsigned(0, ball_y'length) when rst_n = '0' else ball_y_new when en='1' and step_i = '1' and counter_en = '1' and rising_edge(clk);
	ball_y_new <= ball_y + ball_speed_y;
	
	ball_x <= to_unsigned(0, ball_x'length) when rst_n = '0' else ball_x_new when en='1' and step_i = '1' and counter_en = '1' and rising_edge(clk);
	ball_x_new <= ball_x + ball_speed_x;
	
	ball_speed_x <= ball_speed_x_new when rising_edge(clk);
	ball_speed_x_new <= ball_speed_negative when ball_speed_left_i = '1'
					   else ball_speed_positive when ball_speed_right_i = '1'
					   else ball_speed_negative when ball_x >= 640 - ball_size and en = '1'
					   else ball_speed_positive when ball_x = 0 and en = '1'
					   else ball_speed_x;
						
	ball_speed_y <= ball_speed_y_new when rising_edge(clk);
	ball_speed_y_new <= ball_speed_negative when ball_speed_up_i = '1'
					   else ball_speed_positive when ball_speed_down_i = '1'
					   else ball_speed_negative when ball_y >= 480 - ball_size and en = '1'
					   else ball_speed_positive when ball_y = 0 and en = '1'
					   else ball_speed_y;
						
	counter <= to_unsigned(0, 10) when rst_n = '0' else counter_new when en='1' and step_i = '1' and rising_edge(clk);
	counter_new <= to_unsigned(5, 10) when counter = 0
				 else counter - 1;
	
	red_o <= rgb(3 downto 0);
	green_o <= rgb(7 downto 4);
	blue_o <= rgb(11 downto 8);
end architecture;