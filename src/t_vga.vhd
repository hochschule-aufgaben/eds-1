library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity t_vga is
end entity;

architecture tbench of t_vga is

signal clk, rst_n, en : std_ulogic;

signal vga_hs, vga_vs, end_line, end_picture : std_ulogic;
signal current_x, current_y : unsigned(9 downto 0);

signal simstop : boolean := false;

component clockengen is port(
	 clk     : in std_ulogic;
    rst_n   : in std_ulogic;
    en_o   : out std_ulogic
); end component;


component hsync is port(
	clk: in std_ulogic;
	rst_n: in std_logic;
	en: in std_ulogic;
	hsync_o: out std_ulogic;
	end_line_o: out std_logic;
	current_x_o: out unsigned(9 downto 0)
); end component;


component vsync is port(
	clk: in std_ulogic;
	rst_n: in std_logic;
	en: in std_ulogic;
	vsync_o: out std_ulogic;
	end_picture_o: out std_ulogic;
	current_y_o: out unsigned(9 downto 0)
); end component;

begin

  clockengen_i0 : clockengen port map (
    clk => clk,
	 rst_n => rst_n,
	 en_o => en
  );
  
  hsync_i0 : hsync port map(
    clk => clk,
	 rst_n => rst_n,
	 en => en,
	 hsync_o => vga_hs,
	 end_line_o => end_line,
	 current_x_o => current_x
  );
  
  vsync_i0 : vsync port map(
    clk => clk,
	 rst_n => rst_n,
	 en => end_line,
	 vsync_o => vga_vs,
	 end_picture_o => end_picture,
	 current_y_o => current_y
  );

  rst_n <= '0', '1' after 100 ns;

  process begin
    clk <= '0';
    wait for 10 ns;
    clk <= '1';
    wait for 10 ns;
    if simstop then
      wait;
    end if;
  end process;
  
  process begin
    simstop <= false, true after 10 ms;
	 wait;
  end process;


end architecture; 




