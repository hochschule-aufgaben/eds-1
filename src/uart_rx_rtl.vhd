library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_rx is
  port (
    clk     : in std_ulogic;
    rst_n   : in std_ulogic;
    -- Data
    d_o     : out std_ulogic_vector(7 downto 0);
    ser_i   : in std_ulogic; 
    -- Control signals
    dv_o    : out std_ulogic);
end entity;

architecture rtl of uart_rx is
  
  -- Sync
  
  signal sync : std_ulogic_vector(1 downto 0);
  signal ser : std_ulogic;
  
  -- Shift register
  
  signal sr_shift : std_ulogic;
  signal sr, sr_new : std_ulogic_vector(7 downto 0);
  
  -- Baud Counter
  signal baudcnt, baudcntn : unsigned(25 downto 0);
  constant phaseinc_normal : integer := 12885; -- 9600 Baud
  constant phaseinc_double : integer := 12885*2;
  signal phaseinc : integer range phaseinc_normal to phaseinc_double;
  signal phasectrl : std_ulogic;
  signal reset_baudcnt : std_ulogic;
  signal bauden : std_ulogic;
  
  -- TX Control Statemachine
  
  type state_t is (IDLE, START, BIT0, BIT1, BIT2, BIT3,
                   BIT4, BIT5, BIT6, BIT7, STOP);
    
  signal state, nstate : state_t;  

begin
  
  sync <= "11" when rst_n = '0' else sync(0) & ser_i when rising_edge(clk);
  ser <= sync(1);
  
  -- RX Statemachine
  
  state <= IDLE when rst_n = '0' else nstate when rising_edge(clk);
  
nstatelogic_p : process(state, ser, bauden)
begin
  reset_baudcnt <= '0';
  phasectrl <= '0';
  nstate <= state;
  sr_shift <= '0';
  dv_o <= '0';
  case state is
  when IDLE =>
    reset_baudcnt <= '1';
    if ser = '0' then -- Falling edge of start bit
      nstate <= START;
    end if;
  when START =>
    phasectrl <= '1'; -- Double phaseinc for half period
    if bauden = '1' then
      if ser = '0' then -- Check if start bit is 0
        nstate <= BIT0;
      else
        nstate <= IDLE;
      end if;
    end if;
  when BIT0 =>
    if bauden = '1' then
      nstate <= BIT1;
      sr_shift <= '1';
    end if;
  when BIT1 =>
    if bauden = '1' then
      nstate <= BIT2;
      sr_shift <= '1';
    end if;
  when BIT2 =>
    if bauden = '1' then
      nstate <= BIT3;
      sr_shift <= '1';
    end if;
  when BIT3 =>
    if bauden = '1' then
      nstate <= BIT4;
      sr_shift <= '1';
    end if;
  when BIT4 =>
    if bauden = '1' then
      nstate <= BIT5;
      sr_shift <= '1';
    end if;
  when BIT5 =>
    if bauden = '1' then
      nstate <= BIT6;
      sr_shift <= '1';
    end if;
  when BIT6 =>
    if bauden = '1' then
      nstate <= BIT7;
      sr_shift <= '1';
    end if;
  when BIT7 =>
    if bauden = '1' then
      nstate <= STOP;
      sr_shift <= '1';
    end if;
  when STOP =>
    if bauden = '1' then
      nstate <= IDLE;
      dv_o <= '1';
    end if;
  when others =>
  end case;
  
end process;
  
  -- shift register
  sr <= (others => '0') when rst_n = '0' else sr_new when rising_edge(clk);
  sr_new <= ser & sr(7 downto 1) when sr_shift = '1' else
            sr;
  d_o <= sr;
  
  -- Baud Counter
  baudcnt <= to_unsigned(0,baudcnt'length) when rst_n = '0' else baudcntn when rising_edge(clk);
  baudcntn <= to_unsigned(0,baudcnt'length) when reset_baudcnt = '1' else
              baudcnt + phaseinc;   
  bauden <= '1' when baudcntn < baudcnt else '0';
  phaseinc <= phaseinc_normal when phasectrl = '0' else phaseinc_double;

end architecture rtl; 




