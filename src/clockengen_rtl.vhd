library ieee;
use ieee.std_logic_1164.all; 

entity clockengen is
  port (
    clk     : in std_ulogic;
    rst_n   : in std_ulogic;
    en_o   : out std_ulogic
  );
end entity;

architecture rtl of clockengen is

signal latch : std_ulogic;
begin
latch <= '0' when rst_n = '0' else not latch when rising_edge(clk);
en_o <= latch;
end architecture rtl; 




