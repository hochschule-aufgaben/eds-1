library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity t_de1_vga is
end entity;

architecture tbench of t_de1_vga is

signal clk, rst_n : std_ulogic;

signal vga_hs, vga_vs : std_ulogic;

signal simstop : boolean := false;


component de1_vga is port ( 
		 CLOCK_50 : in  std_ulogic;
       LEDG     : out std_ulogic_vector(7 downto 0);   -- green LEDs
       LEDR     : out std_ulogic_vector(9 downto 0);
		 KEY		 : in  std_ulogic_vector(3 downto 0);
		 VGA_R    : out std_ulogic_vector(3 downto 0);
		 VGA_G    : out std_ulogic_vector(3 downto 0);
		 VGA_B    : out std_ulogic_vector(3 downto 0);
		 VGA_HS   : out std_ulogic;
		 VGA_VS   : out std_ulogic;
		 GPIO_0   : out std_ulogic_vector(10 downto 0);
		 GPIO_1   : out std_ulogic_vector(10 downto 0);
		 UART_RXD : in  std_ulogic;
		 UART_TXD : out std_ulogic
		);  -- red LEDs
end component;

begin

  de1_i0 : de1_vga port map (
    CLOCK_50 => clk,
	 VGA_HS => vga_hs,
	 VGA_VS => vga_vs,
	 KEY(0) => rst_n,
	 KEY(3 downto 1) => "000",
	 UART_RXD => '1'
  );

  rst_n <= '0', '1' after 100 ns;

  process begin
    clk <= '0';
    wait for 10 ns;
    clk <= '1';
    wait for 10 ns;
    if simstop then
      wait;
    end if;
  end process;
  
  process begin
    -- simstop <= false, true after 10 ms;
	 wait;
  end process;


end architecture; 




