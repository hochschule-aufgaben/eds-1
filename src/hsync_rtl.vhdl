library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity hsync is port(
	clk: in std_ulogic;
	rst_n: in std_logic;
	en: in std_ulogic;
	hsync_o: out std_ulogic;
	end_line_o: out std_logic;
	current_x_o: out unsigned(9 downto 0)
); end entity;

architecture rtl of hsync is 
	signal current_count, current_count_new : unsigned(9 downto 0);
begin 
	current_count <= to_unsigned(0, current_count'length) when rst_n = '0' else current_count_new when rising_edge(clk) and en = '1';
	current_count_new <= current_count + 1 when current_count < 800 else to_unsigned(0, current_count_new'length);
	
	hsync_o <= '1' when rst_n = '0' else '0' when current_count >= 656 and current_count < 752 else '1';
	end_line_o <= '1' when current_count = 752 and en = '1' else '0';
	
	current_x_o <= current_count;
end architecture rtl;
