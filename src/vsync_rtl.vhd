library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity vsync is port (
	clk: in std_ulogic;
	rst_n: in std_logic;
	en: in std_ulogic;
	vsync_o: out std_ulogic;
	end_picture_o: out std_ulogic;
	current_y_o: out unsigned(9 downto 0)
); end entity;

architecture rtl of vsync is
	signal current_count, current_count_new : unsigned(9 downto 0);
begin
	current_count <= to_unsigned(0, current_count'length) when rst_n = '0' else current_count_new when rising_edge(clk) and en = '1';	
	current_count_new <= current_count + 1 when current_count < 525 else to_unsigned(0, current_count_new'length);

	vsync_o <= '1' when rst_n = '0' else '0' when current_count >= 490 and current_count < 492 else '1';
	end_picture_o <= '1' when current_count = 524 and en = '1' else '0';
	
	current_y_o <= current_count;
end architecture;