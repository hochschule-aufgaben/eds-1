library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library altera_mf;
use altera_mf.all; 
--use altera_mf.altera_mf_components.all;

-- Combinational functions computed from switch values shown at the green leds
entity de1_vga is 
port ( CLOCK_50 : in  std_ulogic;
       LEDG     : out std_ulogic_vector(7 downto 0);   -- green LEDs
       LEDR     : out std_ulogic_vector(9 downto 0);
		 KEY		 : in  std_ulogic_vector(3 downto 0);
		 VGA_R    : out std_ulogic_vector(3 downto 0);
		 VGA_G    : out std_ulogic_vector(3 downto 0);
		 VGA_B    : out std_ulogic_vector(3 downto 0);
		 VGA_HS   : out std_ulogic;
		 VGA_VS   : out std_ulogic;
		 GPIO_0   : out std_ulogic_vector(10 downto 0);
		 GPIO_1   : out std_ulogic_vector(10 downto 0);
		 UART_RXD : in  std_ulogic;
		 UART_TXD : out std_ulogic
		);  -- red LEDs
end entity;

architecture rtl of de1_vga is

signal enable_25: std_logic;
signal current_x, current_y : unsigned(9 downto 0);
signal r, g, b : std_ulogic_vector(3 downto 0);
signal rst : std_ulogic;
signal hsync_end_of_line : std_ulogic;
signal hsync_out : std_ulogic;
signal vsync_end_of_picture : std_ulogic;
signal uart_data_ready : std_ulogic;
signal uart_data : std_ulogic_vector(7 downto 0);
signal led_out : std_ulogic_vector(3 downto 0);
signal ball_speed_left, ball_speed_right : std_ulogic;
signal ball_speed_up, ball_speed_down : std_ulogic;

component clockengen is port(
	 clk     : in std_ulogic;
    rst_n   : in std_ulogic;
    en_o   : out std_ulogic
); end component;

component hsync is port(
	clk: in std_ulogic;
	rst_n: in std_logic;
	en: in std_ulogic;
	hsync_o: out std_ulogic;
	end_line_o: out std_logic;
	current_x_o: out unsigned(9 downto 0)
); end component;

component vsync is port(
	clk: in std_ulogic;
	rst_n: in std_logic;
	en: in std_ulogic;
	vsync_o: out std_ulogic;
	end_picture_o: out std_ulogic;
	current_y_o: out unsigned(9 downto 0)
); end component;

component image_generator is port (
	clk: in std_ulogic;
	en : in std_ulogic;
	rst_n: in std_logic;
	current_x_i : in unsigned(9 downto 0);
	current_y_i : in unsigned(9 downto 0);
	red_o: out std_ulogic_vector(3 downto 0);
	green_o: out std_ulogic_vector(3 downto 0);
	blue_o: out std_ulogic_vector(3 downto 0);
	step_i: in std_ulogic;
	ball_speed_up_i : in std_ulogic;
	ball_speed_down_i : in std_ulogic;
	ball_speed_left_i : in std_ulogic;
	ball_speed_right_i : in std_ulogic
); end component;

component uart_rx is port (
    clk     : in std_ulogic;
    rst_n   : in std_ulogic;
    -- Data
    d_o     : out std_ulogic_vector(7 downto 0);
    ser_i   : in std_ulogic; 
    -- Control signals
    dv_o    : out std_ulogic); end component;

begin

	rst <= KEY(0);
	
	uart_rx_i0 : uart_rx port map (
		clk => CLOCK_50,
		rst_n => rst,
		ser_i => UART_RXD,
		dv_o => uart_data_ready,
		d_o => uart_data
	);

	clockgen_i0 : clockengen port map(
		clk => CLOCK_50,
		rst_n => rst,
		en_o => enable_25
	);
	
	hsync_i0 : hsync port map(
		clk => CLOCK_50,
		rst_n => rst,
		en => enable_25,
		hsync_o => hsync_out,
		current_x_o => current_x,
		end_line_o => hsync_end_of_line
	);
	
	VGA_HS <= hsync_out;
	
	GPIO_0(0) <= hsync_out;
	
	vsync_i0 : vsync port map(
		clk => CLOCK_50,
		rst_n => rst,
		en => hsync_end_of_line,
		vsync_o => VGA_VS,
		current_y_o => current_y,
		end_picture_o => vsync_end_of_picture
	);
	
	image_generator_i0 : image_generator port map (
		clk => CLOCK_50,
		en => enable_25,
		rst_n => rst,
		current_x_i => current_x,
		current_y_i => current_y,
		red_o => VGA_R,
		green_o => VGA_G,
		blue_o => VGA_B,
		step_i => vsync_end_of_picture,
		ball_speed_left_i => ball_speed_left,
		ball_speed_right_i => ball_speed_right,
		ball_speed_up_i => ball_speed_up,
		ball_speed_down_i => ball_speed_down
	);
	
	GPIO_1(0) <= UART_RXD;
	GPIO_1(1) <= uart_data_ready;
	
	UART_TXD <= UART_RXD;
	
	LEDR(3 downto 0) <= led_out when rising_edge(CLOCK_50) and uart_data_ready = '1';
	led_out <= "1000" when uart_data = "01110111" 
	      else "0100" when uart_data = "01100001"
	      else "0010" when uart_data = "01110011"
	      else "0001" when uart_data = "01100100"
			else "0000";
			
	ball_speed_up <= '1' when uart_data_ready = '1' and uart_data = "01110111" else '0';
	ball_speed_left <= '1' when uart_data_ready = '1' and uart_data = "01100001" else '0';
	ball_speed_down <= '1' when uart_data_ready = '1' and uart_data = "01110011" else '0';
	ball_speed_right <= '1' when uart_data_ready = '1' and uart_data = "01100100" else '0';
 
end architecture rtl;
